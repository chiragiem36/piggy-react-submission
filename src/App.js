import React, { Component } from 'react';
import MutualFunds from './MutualFunds.js';
import './App.css';

import { BrowserRouter as Router, Route} from 'react-router-dom';
import browserHistory from 'history/createBrowserHistory';

const history = browserHistory()

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      search: '',
      response: {},
      searchUrl: 'https://api.piggy.co.in/v2/mf/search/',
      headers: [
      {
        header:  'content-type',
        value: 'application/json'
      }]
    }

    if (window.location.search.length > 0) {
      this.state.search = window.location.search.slice(1)

      const data = {"search": this.state.search, "filters":{"minimum_investments":-1,"minimumInvestment":-1}}

      this.fetchData('POST', this.state.searchUrl,data)
    }

    this.inputHandler = this.inputHandler.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }
  inputHandler (event) {
    const type = event.target.type

    if (type === 'text') {
      
      const value = event.target.value

      if (event.target.name === 'search') {
        
        this.setState({[event.target.name]: value})
        history.push('/search?' + value)

        if (value.length === 0) {
          console.log(false)
          this.setState({response: {}})
          return
        }

        const data = {"search": value, "filters":{"minimum_investments":-1,"minimumInvestment":-1}}

        this.fetchData('POST', this.state.searchUrl, data)
      }
    }
  }

  fetchData (method, url, data) {
    this.xhr(method, url, data)
    .then((res) => {
      this.setState({response: res})
    })
    .catch((err) => {
      console.error(err)
    })
  }

  xhr (method, url, data = null) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()

      xhr.open(method, url, true)
      
      if (this.state.headers && this.state.headers.length > 0) {
        this.state.headers.forEach((header) => {
          xhr.setRequestHeader(header.header, header.value)
        })
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
          resolve(JSON.parse(xhr.response))
        } else if (xhr.readyState === 4 && xhr.status !== 200) {
          reject(new Error('Failed to fetch resource'))
        }
      }

      xhr.send(JSON.stringify(data))
    })
  }

  render() {
    return (
      <Router>
        <div className="row text-center">
          <div id="nav" className="col-xs-12">
            <div className="row">
              <input className="col-xs-12" placeholder="Enter name of mutual fund to search" name="search" value={this.state.search} onChange={this.inputHandler} />
            </div>
          </div>
          <div className="col-xs-12">
              <Route path="/" component={() => <MutualFunds res={this.state.response} search={this.state.search} />} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
