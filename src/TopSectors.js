import React, {Component} from 'react'

class Holdings extends Component {

	render () {

		let funds = Object.keys(this.props.details).map((id) => {

			const fund = this.props.details[id]

			const sectors = fund.holdings.top_5_sectors.values.map((sector, n) => {
				return (
						<td key={n.toString() + '-sector'}>
							<span className="text-primary">{sector.sector}</span><br /> <span className="text-success">{sector.allocation_percentage}%</span><br />{sector.instrument}
						</td>
					)
			})

			return (
					<tr key={fund.mutual_fund.details.scheme_details_id}>
						<td>
							{this.props.names[id]}
						</td>
						{sectors}
					</tr>
				)
		})

		return (
			<div className="comparision col-xs-12 sectors">
				<div className="row">
					<span className="col-xs-6">Top 5 Sectors</span>
				</div>
				<table className="row table table-bordered table-responsive">
				<thead>
					<tr>
						<th>
							Mutual Fund
						</th>
						<th>
							1st
						</th>
						<th>
							2nd
						</th>
						<th>
							3rd
						</th>
						<th>
							4th
						</th>
						<th>
							5th
						</th>
					</tr>
				</thead>
					<tbody>
						{funds}
					</tbody>
				</table>
			</div>
		)
	}
}

export default Holdings