import React, {Component} from 'react'
import Comparision from './Comparision.js';
// import browserHistory from 'history/createBrowserHistory';
// const history = browserHistory()

class MutualFunds extends Component {
	constructor (props) {
		super(props)
		console.log(props)

		let mobile = false, show = true
		
		if (window.innerWidth < 721) {
			mobile = true
			show = false
		}

		this.state = {show, mobile, details: {},search: this.props.search, res: this.props.res, sortBy: 'risk', sortOrder: 1, selectedFunds: {}, names: {}}
		this.inputHandler = this.inputHandler.bind(this);
		this.selectFund = this.selectFund.bind(this);
		this.compare = this.compare.bind(this);
		this.hide = this.hide.bind(this);
	}

	compare () {
		this.setState({show: true})
	}

	hide () {
		this.setState({show: false})
	}

	inputHandler (event) {
	    const type = event.target.type
	    console.log()

	    if (type === 'select-one') {
	      
	      const value = event.target.value
	        
	        this.setState({[event.target.name]: value})
	    }
	  }

	  selectFund (fund) {
	  	let selectedFunds = this.state.selectedFunds
	  	let names = this.state.names

	  	if (selectedFunds[fund.details_id]) {
	  		delete selectedFunds[fund.details_id]
	  		delete names[fund.details_id]
	  		this.setState({selectedFunds})
	  		this.setState({names})
	  		document.getElementById(fund.details_id).style.backgroundColor = 'white'
	  	} else {
	  		document.getElementById(fund.details_id).style.backgroundColor = 'lightblue'
	  		
	  		names[fund.details_id] = fund.name
	  		this.setState({names})
	  		
	  		this.xhr('GET', 'https://api.piggy.co.in/v1/mf/?key=' + fund.details_id)
		    .then((res) => {
		      selectedFunds[fund.details_id] = res.data
		    })
		    .then(() => {
		    	this.setState({selectedFunds})
		    	console.log(this.state.names)
		    })
	  	}
	  }

  xhr (method, url, data = null) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()

      xhr.open(method, url, true)
      
      if (this.state.headers && this.state.headers.length > 0) {
        this.state.headers.forEach((header) => {
          xhr.setRequestHeader(header.header, header.value)
        })
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
          resolve(JSON.parse(xhr.response))
        } else if (xhr.readyState === 4 && xhr.status !== 200) {
          reject(new Error('Failed to fetch resource'))
        }
      }

      xhr.send(JSON.stringify(data))
    })
  }

	render () {

		let Element, component

		if (this.state.search.length === 0) {
						Element = <span className="col-xs-12" id="empty-result">Enter something to search</span>
		} else if (this.state.search.length > 0 && (!this.state.res.data || this.state.res.data.search_results_count === 0)) {
						Element = <span className="col-xs-12" id="empty-result">nothing found</span>
		} else {

			const list = this.state.res.data.search_results.sort((a, b) => {
				const order = this.state.sortOrder

				if (this.state.sortBy === 'rating') {
					return order*(b.rating - a.rating)
				} else if (this.state.sortBy === 'risk') {
					let r = {
						'High': 4,
						'Moderately High': 3,
						'Moderate': 2,
						'Moderately Low': 1,
						'Low': 0,
					}

					let d = r[b.riskometer] - r[a.riskometer]
					if (d > 1) {
						d=1
					} else if (d < -1) {
						d = -1
					}

					return order*d
				} else if (this.state.sortBy === 'min') {
					return order*(b.minimum_investment - a.minimum_investment)
				} else {
					return null
				}
			})

			if (Object.keys(this.state.selectedFunds).length > 0) {
				component = <Comparision names={this.state.names} details={this.state.selectedFunds} />
			} else {
				component = null
			}

			Element = list.map((result, n) => {
				let stars = []
				for (let i=0; i < result.rating; ++i) {
					stars.push(<span key={'icon-' + i} className="glyphicon glyphicon-star text-warning"></span>)
				}
				return (
						<div className="row card mutual-fund" id={result.details_id} onClick={ (e) => this.selectFund(result)} key={result.details_id}>
							<div className="card-heading col-xs-12">
								{result.name}
							</div>
							<div className="card-heading tags col-xs-12">
								<span>#{result.category}</span>
								<span>#{result.sub_category}</span>
							</div>
							<div className="col-xs-11">
								<div className="row investment">
									<span className="col-xs-4">
										min. - <br />INR {result.minimum_investment/1000}K
									</span>
									<span className="col-xs-4">
										3yr return - <span className="text-success">{result.return_3yr}%</span>
									</span>
									<span className="col-xs-4">
										5yr return - <span className="text-success">{result.return_5yr}%</span>
									</span>
								</div>
							</div>
							<div className="card-body col-xs-12">
								<div className="row">
									<span className="col-xs-6 risk">Risk - 
										<span className={result.riskometer.toLowerCase().replace('', '')}>{result.riskometer}</span>
									</span>
									<span className="col-xs-6 risk">Ratings - 
										<span className={result.rating}>{stars}</span>
									</span>
								</div>
							</div>
						</div>
					)
			})
		}

		return (
			<div className="row">
				<button id="compare-button" className="btn btn-primary" onClick={this.compare} style={{display: Object.keys(this.state.selectedFunds).length > 0 && this.state.mobile && !this.state.show ? 'block' : 'none'}}>Show details or compare</button>
				<button id="list-button" className="btn btn-danger" onClick={this.hide} style={{display: this.state.mobile && this.state.show ? 'block' : 'none'}}>Back to list</button>
				<div style={{'display': (this.state.mobile && !this.state.show) || !this.state.mobile ? 'block' : 'none'}} className="col-sm-5 col-md-4 col-lg-3 col-xs-12">
					<div className="row list">
						<div className="col-xs-12">
							<div className="row" id="action-bar">
								<span className="col-xs-5 col-xs-offset-1 sort-by">
									<span>Sort By</span>
									<select name="sortBy" value={this.state.sortBy} onChange={this.inputHandler}>
										<option value="risk">risk</option>
										<option value="rating">rating</option>
										<option value="min">minimum investment</option>
										<option value="max">max investment</option>
									</select>
								</span>
								<span className="col-xs-4 col-xs-offset-1 sort-order">
									<span>Order</span>
									<select name="sortOrder" value={this.state.sortOrder} onChange={this.inputHandler}>
										<option value="1">Descending</option>
										<option value="-1">Ascending</option>
									</select>
								</span>
							</div>
						</div>
						<div className="col-xs-12" style={{maxHeight: window.innerHeight - 91 + 'px', overflowY: 'scroll'}}>
							{Element}
						</div>
					</div>
				</div>
				<div style={{'display': (this.state.mobile && this.state.show) || (!this.state.mobile) ? 'hidden' : 'block'}}>{component}</div>
			</div>
			)
	}
}

export default MutualFunds