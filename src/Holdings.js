import React, {Component} from 'react'

class Holdings extends Component {

	render () {

		let funds = Object.keys(this.props.details).map((id) => {

			const fund = this.props.details[id]

			const holdings = fund.holdings.top_10_holdings.values.map((holding, n) => {
				return (
						<td key={n.toString() + '-company'}>
							<span className="text-primary">{holding.script}</span><br /> <span className="text-success">{holding.allocation_percentage}%</span><br />{holding.instrument}
						</td>
					)
			})

			return (
					<tr key={fund.mutual_fund.details.scheme_details_id}>
						<td>
							{this.props.names[id]}
						</td>
						{holdings}
					</tr>
				)
		})

		return (
			<div className="comparision col-xs-12 holdings">
				<div className="row">
					<span className="col-xs-6">Top 10 Holdings</span>
				</div>
				<table className="row table table-bordered table-responsive">
				<thead>
					<tr>
						<th>
							Mutual Fund
						</th>
						<th>
							1st
						</th>
						<th>
							2nd
						</th>
						<th>
							3rd
						</th>
						<th>
							4th
						</th>
						<th>
							5th
						</th>
						<th>
							6th
						</th>
						<th>
							7th
						</th>
						<th>
							8th
						</th>
						<th>
							9th
						</th>
						<th>
							10th
						</th>
					</tr>
				</thead>
					<tbody>
						{funds}
					</tbody>
				</table>
			</div>
		)
	}
}

export default Holdings