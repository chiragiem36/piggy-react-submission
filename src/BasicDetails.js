import React, {Component} from 'react'

class BasicDetails extends Component {

	render () {

		let funds = Object.keys(this.props.details).map((id) => {
			const fund = this.props.details[id]
			return (
					<tr key={fund.mutual_fund.details.scheme_details_id}>
						<td>
							{this.props.names[id]}
						</td>
						<td>
							{fund.is_tax_saving_fund ? 'Yes' : 'No'}
						</td>
						<td>
							{fund.mutual_fund.dividend_type}
						</td>
						<td>
							{fund.plan_type}
						</td>
						<td>
							{Math.ceil(fund.mutual_fund.best_return.percent_change*10)/10}%, ({fund.mutual_fund.best_return.fromdate} - {fund.mutual_fund.best_return.todate})
						</td>
						<td>
							{fund.mutual_fund.details.rating}
						</td>
						<td>
							{fund.mutual_fund.details.minimum_subscription}
						</td>
						<td>
							{fund.mutual_fund.details.scheme_type}
						</td>
						<td>
							{fund.mutual_fund.details.scheme_class}
						</td>
						<td>
							{fund.mutual_fund.details.minimum_balance_maintainence}
						</td>
						<td>
							{fund.mutual_fund.details.yoy_return}
						</td>
						<td>
							{fund.mutual_fund.details.return_3yr}
						</td>
						<td>
							{fund.mutual_fund.details.return_5yr}
						</td>
						<td>
							{fund.mutual_fund.details.amc.launch_date}
						</td>
					</tr>
				)
		})

		return (
			<div className="comparision col-xs-12 basic-details">
				<div className="row">
					<span className="col-xs-6">Basic Details</span>
				</div>
				<table className="row table table-bordered table-responsive">
				<thead>
					<tr>
						<th>
							Mutual Fund
						</th>
						<th>
							Tax Savings ?
						</th>
						<th>
							Dividend Type
						</th>
						<th>
							Plan Type
						</th>
						<th>
							%age change<br />(from - to)
						</th>
						<th>
							Ratings
						</th>
						<th>
							Min. Subscription
						</th>
						<th>
							Scheme type
						</th>
						<th>
							Scheme class
						</th>
						<th>
							Min Balance
						</th>
						<th>
							YOY Return
						</th>
						<th>
							3year Return
						</th>
						<th>
							5year Return
						</th>
						<th>
							launch
						</th>
					</tr>
				</thead>
					<tbody>
						{funds}
					</tbody>
				</table>
			</div>
		)
	}
}

export default BasicDetails