import React, {Component} from 'react'
import BasicDetails from './BasicDetails.js'
import Holdings from './Holdings.js'
import TopSectors from './TopSectors.js'

class Comparision extends Component {
	constructor (props) {
		super(props)
		console.log(props)
		this.state = {details: this.props.details}
	}

	render () {
		return (
			<div style={{maxHeight: window.innerHeight - 50 + 'px', overflowY: 'scroll'}} className="comparision col-sm-7 col-md-8 col-lg-9 col-xs-12">
				<div className="row">
					<BasicDetails names={this.props.names} details={this.props.details} />
					<Holdings names={this.props.names} details={this.props.details} />
					<TopSectors names={this.props.names} details={this.props.details} />
				</div>
			</div>
		)
	}
}

export default Comparision